#include "GV.h"

void firstini(){
	//キーコンフィグの代入
	configpad.down=tempkey[0];
	configpad.left=tempkey[1];
	configpad.right=tempkey[2];
	configpad.up=tempkey[3];
	configpad.circle=tempkey[4];
	configpad.cross=tempkey[5];
	configpad.square=tempkey[6];
	configpad.triangle=tempkey[7];
	configpad.start=tempkey[12];

	//初期
	// 描画位置の初期位置セット
	CursorX = 0 ;
	CursorY = 0 ;
	
	// 参照文字位置をセット
	SP = 0 ;	// １行目の
	CP = 0 ;	// ０文字

	// フレームカウンタ初期化
	KeyWaitFlag = 1;
	count=0;
	opkey=0;
	event=0;
    battleflag=0;

	// 描画する文字列のサイズを設定
	SetFontSize( MOJI_SIZE ) ;
	color[0] =GetColor(255,255,255);
	color[1] =GetColor(255,0,0);
	color[2] =GetColor(0,0,255);
	color[3]=GetColor( 0 , 255 , 0 ) ;
	color[4]=GetColor( 0 , 0 , 0 ) ;

	srand((unsigned) time(NULL));


	//キャラネーム
	strcpy(CharName[0],"");
	strcpy(CharName[1],"ノギリ");	
	strcpy(CharName[2],"イノハラ");
	strcpy(CharName[3],"ドノウエ");
	strcpy(CharName[4],"田嶋");
}

//ノベル開始前初期化
void novelini(){
	int i,j;
	CP=0;
	EndFlag=0;
	// 仮想テキストバッファを初期化して描画文字位置を初期位置に戻すおよび参照文字位置を一つ進める
	for( i = 0 ; i < STRBUF_HEIGHT ; i ++ )
	{
		for( j = 0 ; j < STRBUF_WIDTH * 2 ; j ++ )
		{
			StringBuf[ i ][ j ] = 0 ;
		}
	}
	// 描画位置の初期位置セット
	CursorX = 0 ;
	CursorY = 0 ;
	// フレームカウンタ初期化
	KeyWaitFlag = 0;
	count=0;
	CharNum=0;
	LRFlag=0;

}



void ini(){
	switch(event){
	case 0://序章イベント
		novelini();
		func_state=100;//novel
		break;
	case 1://一面へ
		break;
	}
}


