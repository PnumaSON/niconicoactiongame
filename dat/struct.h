//パッドに関する構造体
typedef struct{
	int key[16];
}pad_t;

//コンフィグに関する構造体
typedef struct{
	int left,up,right,down,circle,cross,square,triangle,start;
}configpad_t;

// プレイヤー情報構造体
struct PLAYER
{
	VECTOR		Position ;				// 座標
	VECTOR		TargetMoveDirection ;			// モデルが向くべき方向のベクトル
	float		Angle ;					// モデルが向いている方向の角度
	float		JumpPower ;				// Ｙ軸方向の速度
	int		ModelHandle ;				// モデルハンドル
	int		ShadowHandle ;				// 影画像ハンドル
	int		State ;					// 状態( 0:立ち止まり  1:走り  2:ジャンプ )

	int		PlayAnim1 ;				// 再生しているアニメーション１のアタッチ番号( -1:何もアニメーションがアタッチされていない )
	float		AnimPlayCount1 ;			// 再生しているアニメーション１の再生時間
	int		PlayAnim2 ;				// 再生しているアニメーション２のアタッチ番号( -1:何もアニメーションがアタッチされていない )
	float		AnimPlayCount2 ;			// 再生しているアニメーション２の再生時間
	float		AnimBlendRate ;				// 再生しているアニメーション１と２のブレンド率
};

// カメラ情報構造体
struct CAMERA
{
	float		AngleH ;				// 水平角度
	float		AngleV ;				// 垂直角度
	VECTOR		Eye ;					// カメラ座標
	VECTOR		Target ;				// 注視点座標
} ;