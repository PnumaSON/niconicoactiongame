#include "GV.h"

// プレイヤーに新たなアニメーションを再生する
void Player_PlayAnim( int num,int PlayAnim )
{
	// 再生中のモーション２が有効だったらデタッチする
	if( pl[num].PlayAnim2 != -1 )
	{
		MV1DetachAnim( pl[num].ModelHandle, pl[num].PlayAnim2 ) ;
		pl[num].PlayAnim2 = -1 ;
	}

	// 今まで再生中のモーション１だったものの情報を２に移動する
	pl[num].PlayAnim2 = pl[num].PlayAnim1 ;
	pl[num].AnimPlayCount2 = pl[num].AnimPlayCount1 ;

	// 新たに指定のモーションをモデルにアタッチして、アタッチ番号を保存する
	pl[num].PlayAnim1 = MV1AttachAnim( pl[num].ModelHandle, PlayAnim ) ;
	pl[num].AnimPlayCount1 = 0.0f ;

	// ブレンド率は再生中のモーション２が有効ではない場合は１．０ｆ( 再生中のモーション１が１００％の状態 )にする
	pl[num].AnimBlendRate = pl[num].PlayAnim2 == -1 ? 1.0f : 0.0f ;
}

// プレイヤーのアニメーション処理
void Player_AnimProcess( int num )
{
	float AnimTotalTime ;		// 再生しているアニメーションの総時間

	// ブレンド率が１以下の場合は１に近づける
	if( pl[num].AnimBlendRate < 1.0f )
	{
		pl[num].AnimBlendRate += PLAYER_ANIM_BLEND_SPEED ;
		if( pl[num].AnimBlendRate > 1.0f )
		{
			pl[num].AnimBlendRate = 1.0f ;
		}
	}

	// 再生しているアニメーション１の処理
	if( pl[num].PlayAnim1 != -1 )
	{
		// アニメーションの総時間を取得
		AnimTotalTime = MV1GetAttachAnimTotalTime( pl[num].ModelHandle, pl[num].PlayAnim1 ) ;

		// 再生時間を進める
		pl[num].AnimPlayCount1 += PLAYER_PLAY_ANIM_SPEED ;

		// 再生時間が総時間に到達していたら再生時間をループさせる
		if( pl[num].AnimPlayCount1 >= AnimTotalTime )
		{
			pl[num].AnimPlayCount1 = fmod( pl[num].AnimPlayCount1, AnimTotalTime ) ;
		}

		// 変更した再生時間をモデルに反映させる
		MV1SetAttachAnimTime( pl[num].ModelHandle, pl[num].PlayAnim1, pl[num].AnimPlayCount1 ) ;

		// アニメーション１のモデルに対する反映率をセット
		MV1SetAttachAnimBlendRate( pl[num].ModelHandle, pl[num].PlayAnim1, pl[num].AnimBlendRate ) ;
	}

	// 再生しているアニメーション２の処理
	if( pl[num].PlayAnim2 != -1 )
	{
		// アニメーションの総時間を取得
		AnimTotalTime = MV1GetAttachAnimTotalTime( pl[num].ModelHandle, pl[num].PlayAnim2 ) ;

		// 再生時間を進める
		pl[num].AnimPlayCount2 += PLAYER_PLAY_ANIM_SPEED ;

		// 再生時間が総時間に到達していたら再生時間をループさせる
		if( pl[num].AnimPlayCount2 > AnimTotalTime )
		{
			pl[num].AnimPlayCount2 = fmod( pl[num].AnimPlayCount2, AnimTotalTime ) ;
		}

		// 変更した再生時間をモデルに反映させる
		MV1SetAttachAnimTime( pl[num].ModelHandle, pl[num].PlayAnim2, pl[num].AnimPlayCount2 ) ;

		// アニメーション２のモデルに対する反映率をセット
		MV1SetAttachAnimBlendRate( pl[num].ModelHandle, pl[num].PlayAnim2, 1.0f - pl[num].AnimBlendRate ) ;
	}
}

// プレイヤーの影を描画
//void Player_ShadowRender( void )
//{
//	int i, j ;
//	MV1_COLL_RESULT_POLY_DIM HitResDim ;
//	MV1_COLL_RESULT_POLY *HitRes ;
//	VERTEX3D Vertex[ 3 ] ;
//	VECTOR SlideVec ;
//	int ModelHandle ;
//
//	// ライティングを無効にする
//	SetUseLighting( FALSE ) ;
//
//	// Ｚバッファを有効にする
//	SetUseZBuffer3D( TRUE ) ;
//
//	// テクスチャアドレスモードを CLAMP にする( テクスチャの端より先は端のドットが延々続く )
//	SetTextureAddressMode( DX_TEXADDRESS_CLAMP ) ;
//
//	// 影を落とすモデルの数だけ繰り返し
//	for( j = 0 ; j < stg.CollObjNum + 1 ; j ++ )
//	{
//		// チェックするモデルは、jが0の時はステージモデル、1以上の場合はコリジョンモデル
//		if( j == 0 )
//		{
//			ModelHandle = stg.ModelHandle ;
//		}
//		else
//		{
//			ModelHandle = stg.CollObjModelHandle[ j - 1 ] ;
//		}
//
//		// プレイヤーの直下に存在する地面のポリゴンを取得
//		HitResDim = MV1CollCheck_Capsule( ModelHandle, -1, pl.Position, VAdd( pl.Position, VGet( 0.0f, -PLAYER_SHADOW_HEIGHT, 0.0f ) ), PLAYER_SHADOW_SIZE ) ;
//
//		// 頂点データで変化が無い部分をセット
//		Vertex[ 0 ].dif = GetColorU8( 255,255,255,255 ) ;
//		Vertex[ 0 ].spc = GetColorU8( 0,0,0,0 ) ;
//		Vertex[ 0 ].su = 0.0f ;
//		Vertex[ 0 ].sv = 0.0f ;
//		Vertex[ 1 ] = Vertex[ 0 ] ;
//		Vertex[ 2 ] = Vertex[ 0 ] ;
//
//		// 球の直下に存在するポリゴンの数だけ繰り返し
//		HitRes = HitResDim.Dim ;
//		for( i = 0 ; i < HitResDim.HitNum ; i ++, HitRes ++ )
//		{
//			// ポリゴンの座標は地面ポリゴンの座標
//			Vertex[ 0 ].pos = HitRes->Position[ 0 ] ;
//			Vertex[ 1 ].pos = HitRes->Position[ 1 ] ;
//			Vertex[ 2 ].pos = HitRes->Position[ 2 ] ;
//
//			// ちょっと持ち上げて重ならないようにする
//			SlideVec = VScale( HitRes->Normal, 0.5f ) ;
//			Vertex[ 0 ].pos = VAdd( Vertex[ 0 ].pos, SlideVec ) ;
//			Vertex[ 1 ].pos = VAdd( Vertex[ 1 ].pos, SlideVec ) ;
//			Vertex[ 2 ].pos = VAdd( Vertex[ 2 ].pos, SlideVec ) ;
//
//			// ポリゴンの不透明度を設定する
//			Vertex[ 0 ].dif.a = 0 ;
//			Vertex[ 1 ].dif.a = 0 ;
//			Vertex[ 2 ].dif.a = 0 ;
//			if( HitRes->Position[ 0 ].y > pl.Position.y - PLAYER_SHADOW_HEIGHT )
//				Vertex[ 0 ].dif.a = 128 * ( 1.0f - fabs( HitRes->Position[ 0 ].y - pl.Position.y ) / PLAYER_SHADOW_HEIGHT ) ;
//
//			if( HitRes->Position[ 1 ].y > pl.Position.y - PLAYER_SHADOW_HEIGHT )
//				Vertex[ 1 ].dif.a = 128 * ( 1.0f - fabs( HitRes->Position[ 1 ].y - pl.Position.y ) / PLAYER_SHADOW_HEIGHT ) ;
//
//			if( HitRes->Position[ 2 ].y > pl.Position.y - PLAYER_SHADOW_HEIGHT )
//				Vertex[ 2 ].dif.a = 128 * ( 1.0f - fabs( HitRes->Position[ 2 ].y - pl.Position.y ) / PLAYER_SHADOW_HEIGHT ) ;
//
//			// ＵＶ値は地面ポリゴンとプレイヤーの相対座標から割り出す
//			Vertex[ 0 ].u = ( HitRes->Position[ 0 ].x - pl.Position.x ) / ( PLAYER_SHADOW_SIZE * 2.0f ) + 0.5f ;
//			Vertex[ 0 ].v = ( HitRes->Position[ 0 ].z - pl.Position.z ) / ( PLAYER_SHADOW_SIZE * 2.0f ) + 0.5f ;
//			Vertex[ 1 ].u = ( HitRes->Position[ 1 ].x - pl.Position.x ) / ( PLAYER_SHADOW_SIZE * 2.0f ) + 0.5f ;
//			Vertex[ 1 ].v = ( HitRes->Position[ 1 ].z - pl.Position.z ) / ( PLAYER_SHADOW_SIZE * 2.0f ) + 0.5f ;
//			Vertex[ 2 ].u = ( HitRes->Position[ 2 ].x - pl.Position.x ) / ( PLAYER_SHADOW_SIZE * 2.0f ) + 0.5f ;
//			Vertex[ 2 ].v = ( HitRes->Position[ 2 ].z - pl.Position.z ) / ( PLAYER_SHADOW_SIZE * 2.0f ) + 0.5f ;
//
//			// 影ポリゴンを描画
//			DrawPolygon3D( Vertex, 1, pl.ShadowHandle, TRUE ) ;
//		}
//
//		// 検出した地面ポリゴン情報の後始末
//		MV1CollResultPolyDimTerminate( HitResDim ) ;
//	}
//
//	// ライティングを有効にする
//	SetUseLighting( TRUE ) ;
//
//	// Ｚバッファを無効にする
//	SetUseZBuffer3D( FALSE ) ;
//}


// プレイヤーの初期化
void Player_Initialize(){
	int i;

	for(i=0;i<2;i++){
		// 初期座標は原点
		pl[i].Position = VGet( 0.0f, 10.0f, -17.0f+i*34.0 ) ;

		// 回転値は０
		pl[i].Angle = 0.0f ;

		// ジャンプ力は初期状態では０
		pl[i].JumpPower = 0.0f ;

		// モデルの読み込み
		pl[i].ModelHandle = MV1LoadModel( "data/3D/nico1kai.mv1" ) ;

		// 影描画用の画像の読み込み
		//pl[i].ShadowHandle = LoadGraph( "Shadow.tga" ) ;

		// 初期状態では「立ち止り」状態
		pl[i].State = 0 ;

		// 初期状態でプレイヤーが向くべき方向はx軸方向
		pl[i].TargetMoveDirection = VGet( 1.0f, 0.0f, 0.0f ) ;
		MV1SetRotationXYZ( pl[i].ModelHandle, VGet( 0.0f, atan2( pl[i].TargetMoveDirection.x, pl[i].TargetMoveDirection.z ) + DX_PI_F, 0.0f ) ) ;
		// アニメーションのブレンド率を初期化
		pl[i].AnimBlendRate = 1.0f ;

		// 初期状態ではアニメーションはアタッチされていないにする
		pl[i].PlayAnim1 = -1 ;
		pl[i].PlayAnim2 = -1 ;

		// ただ立っているアニメーションを再生
		Player_PlayAnim( i,0 ) ;
	}
}

// プレイヤーの後始末
void Player_Terminate()
{
	int i;
	for(i=0;i<2;i++){
		// モデルの削除
		MV1DeleteModel( pl[i].ModelHandle ) ;

		// 影用画像の削除
		DeleteGraph( pl[i].ShadowHandle ) ;
	}
}

// プレイヤーの処理
void Player_Process( int num )
{

	// アニメーション処理
	Player_AnimProcess(num) ;
	//ポジション
	MV1SetPosition( pl[num].ModelHandle, pl[num].Position ) ;
}

// カメラの初期化処理
void Camera_Initialize()
{
	MATRIX RotZ, RotY ;
	float Camera_Player_Length ;

	SetupCamera_Ortho(50);  
	// カメラの初期水平角度は１８０度
	cam.AngleH = DX_PI_F ;

	// 垂直角度は０度
	cam.AngleV = 0.0f ;
	// カメラの注視点
	cam.Target = VGet( 0.0f, CAMERA_PLAYER_TARGET_HEIGHT, 0.0f ) ;
	// 水平方向の回転はＹ軸回転
	RotY = MGetRotY( cam.AngleH ) ;

	// 垂直方向の回転はＺ軸回転 )
	RotZ = MGetRotZ( cam.AngleV ) ;

	// カメラからプレイヤーまでの初期距離をセット
	Camera_Player_Length = CAMERA_PLAYER_LENGTH ;

	// カメラの座標を算出
	// Ｘ軸にカメラとプレイヤーとの距離分だけ伸びたベクトルを
	// 垂直方向回転( Ｚ軸回転 )させたあと水平方向回転( Ｙ軸回転 )して更に
	// 注視点の座標を足したものがカメラの座標
	cam.Eye = VAdd( VTransform( VTransform( VGet( -Camera_Player_Length, 0.0f, 0.0f ), RotZ ), RotY ), cam.Target ) ;


	// カメラの情報をライブラリのカメラに反映させる
	SetCameraPositionAndTarget_UpVecY( cam.Eye, cam.Target ) ;

}
// カメラの処理
void Camera_Process(){
	MATRIX RotZ, RotY ;
	float Camera_Player_Length ;

	// 水平方向の回転はＹ軸回転
	RotY = MGetRotY( cam.AngleH ) ;

	// 垂直方向の回転はＺ軸回転 )
	RotZ = MGetRotZ( cam.AngleV ) ;

	// カメラからプレイヤーまでの初期距離をセット
	Camera_Player_Length = CAMERA_PLAYER_LENGTH ;
	// カメラの座標を算出
	// Ｘ軸にカメラとプレイヤーとの距離分だけ伸びたベクトルを
	// 垂直方向回転( Ｚ軸回転 )させたあと水平方向回転( Ｙ軸回転 )して更に
	// 注視点の座標を足したものがカメラの座標
	cam.Eye = VAdd( VTransform( VTransform( VGet( -Camera_Player_Length, 0.0f, 0.0f ), RotZ ), RotY ), cam.Target ) ;

	SetCameraNearFar( 20.0f, 1500.0f ) ;
	// カメラの情報をライブラリのカメラに反映させる
	SetCameraPositionAndTarget_UpVecY( cam.Eye, cam.Target ) ;
}
// 描画処理
void Render_Process()
{
	int i ;

	//// ステージモデルの描画
	//MV1DrawModel( stg.ModelHandle ) ;

	//// コリジョンモデルの描画
	//for( i = 0 ; i < stg.CollObjNum ; i ++ )
	//{
	//	MV1DrawModel( stg.CollObjModelHandle[ i ] ) ;
	//}

	// プレイヤーモデルの描画
	MV1DrawModel( pl[0].ModelHandle ) ;
	MV1DrawModel( pl[1].ModelHandle ) ;
	// プレイヤーの影の描画
	//Player_ShadowRender() ;
}