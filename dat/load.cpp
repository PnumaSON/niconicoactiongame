#include "GV.h"


//キー設定ファイルのロード
void keyfileload(){
	FILE *fp ;
	int i;
	char temp[3];
	//もしファイルがなければ
	if( (fp=fopen( "key.dat","rb" ))==NULL ){
		for(i=0;i<14;i++){
			tempkey[i]=i;
		}
	}
	else{
		for(i=0;i<14;i++){
			fgets(temp,6,fp);
			tempkey[i]=atoi(temp);
		}
		fclose( fp ) ;
	}

}

//novelfileロード
void novelload(){


}


void load(){

	char tmp[30];
	int i;

	img_board[0] = LoadGraph("data/back.png");
	img_board[1] = LoadGraph("data/title.png");
	img_board[2] = LoadGraph("data/start.png");

	img_board[3] = LoadGraph("data/battle.png");

	//メッセボックス
	img_board[7]	= LoadGraph( "data/novel/messe.png" );
	img_board[8]	= LoadGraph( "data/novel/messeao.png" );

	img_battle[0]	= LoadGraph( "data/Gofight.png" );
	img_battle[1]	= LoadGraph( "data/HP.png" );
	img_battle[2]	= LoadGraph( "data/you.png" );
	img_battle[3]	= LoadGraph( "data/ene.png" );
	img_battle[4]	= LoadGraph( "data/win.png" );
	img_battle[5]	= LoadGraph( "data/lose.png" );
	img_battle[6]	= LoadGraph( "data/draw.png" );

	//card load
	for(i=1;i<6;i++){
		sprintf(tmp,"data/card/ken%d.png",i);
		img_card[0][i-1] = LoadGraph(tmp);
		sprintf(tmp,"data/card/tate%d.png",i);
		img_card[1][i-1] = LoadGraph(tmp);
		sprintf(tmp,"data/card/tue%d.png",i);
		img_card[2][i-1] = LoadGraph(tmp);
	}
	img_card[0][5]	=LoadGraph("data/card/ken.png");
	img_card[1][5]	=LoadGraph("data/card/tate.png");
	img_card[2][5]	=LoadGraph("data/card/tue.png");
	img_waku        =LoadGraph("data/card/waku.png");

	//キャラ立ち絵
	img_char[1]		= LoadGraph( "data/char/cha1.png" );
	img_char[2]		= LoadGraph( "data/char/cha2.png" );
	img_char[3]		= LoadGraph( "data/char/cha3.png" );

	sound_se[0]=LoadSoundMem("data/shu.wav");
	sound_se[1]=LoadSoundMem("data/pepepin.wav");
	sound_se[2]=LoadSoundMem("data/gofight.wav");

	sound_bgm[0]=LoadSoundMem("data/nico1loop.wav");
	sound_bgm[1]=LoadSoundMem("data/win.wav");
	sound_bgm[2]=LoadSoundMem("data/lose.wav");
	sound_bgm[3]=LoadSoundMem("data/draw.wav");
	//キーコンフィグ
	keyfileload();
}
