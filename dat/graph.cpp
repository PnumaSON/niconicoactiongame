#include "GV.h"


void cardgraph(){
	int i;


	//カードセレクトの表示
	DrawRotaGraph( 1024/10*(bselector[0]*2+1) , 768*5/6 , 0.5f, 0.0f,img_waku , TRUE , FALSE) ;

	//カードの表示
	for(i=0;i<5;i++){
		if(nowcard[0][i]!=-1){
			DrawRotaGraph( 1024/10*(i*2+1) , 768*5/6 , 0.5f, 0.0f,img_card[(nowcard[0][i]-1)/5][(nowcard[0][i]-1)%5] , TRUE , FALSE) ;
		}
	}



	
}


void battlegraph(){

	DrawExtendGraph( 0 , 0 , 1024 , 768 , img_board[0] , FALSE ) ;
	Player_Process(0);
	Player_Process(1);
	Camera_Process();
	Render_Process();

	//カードの枠とか
	DrawLine( 512 , 0 , 512 , 768*2/3 , color[4] ) ;
	DrawLine( 0 , 768*2/3 , 1024 , 768*2/3 , color[4] ) ;

	//カードの表示
	cardgraph();

	//HP表示
	DrawRotaGraph( 1024/4-40 , 35 , 1.0f, 0.0f,img_battle[1] , TRUE , FALSE) ;
	DrawBox( 41 , 21 , 41+351*hp[0]/MAX_HP , 50 , color[2] , TRUE) ;
	DrawFormatString( 430, 20, color[4], "%d", hp[0] ) ;
	DrawRotaGraph( 1024/4*3-40 , 35 , 1.0f, 0.0f,img_battle[1] , TRUE , FALSE) ;
	DrawBox( 553 , 21 , 553+351*hp[1]/MAX_HP , 50 , color[2] , TRUE) ;
	DrawFormatString( 942, 20, color[4], "%d", hp[1] ) ;

}