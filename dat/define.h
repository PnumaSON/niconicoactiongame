// 文字のサイズ
#define MOJI_SIZE 36

// 仮想テキストバッファの横サイズ縦サイズ
#define STRBUF_WIDTH	20
#define STRBUF_HEIGHT	5

#define MouseLeft		0
#define MouseRight		1

//ゲーム用

// プレイヤー関係の定義
#define PLAYER_PLAY_ANIM_SPEED			0.5f		// アニメーション速度
#define PLAYER_ANIM_BLEND_SPEED			0.05f		// アニメーションのブレンド率変化速度
#define PLAYER_SHADOW_SIZE			200.0f		// 影の大きさ
#define PLAYER_SHADOW_HEIGHT			700.0f		// 影が落ちる高さ
// カメラ関係の定義
#define CAMERA_ANGLE_SPEED			0.05f		// 旋回速度
#define CAMERA_PLAYER_TARGET_HEIGHT		15.0f		// プレイヤー座標からどれだけ高い位置を注視点とするか
#define CAMERA_PLAYER_LENGTH			40.0f		// プレイヤーとの距離
#define CAMERA_COLLISION_SIZE			50.0f		// カメラの当たり判定サイズ

#define MAX_HP 15