#include "DxLib.h"
#include "struct.h"
#include "define.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#ifdef GLOBAL_INSTANCE
#define GLOBAL
#else
#define GLOBAL extern 
#endif

#include "function.h"           //関数宣言

#define PI	3.1415926535897932384626433832795f
//ふらぐ
GLOBAL int func_state,count;

GLOBAL int opkey;//opのカーソルの状態
//音楽ファイル用変数宣言部
GLOBAL int sound_se[30];
GLOBAL int sound_bgm[10];
GLOBAL int voice[30];
//画像用変数宣言部
GLOBAL int img_board[40];	//ボードにつかう画像
GLOBAL int img_op1[15];
GLOBAL int img_op2[15];
GLOBAL int img_card[3][6];
GLOBAL int img_waku;
GLOBAL int img_battle[20];
GLOBAL int img_char[20];

GLOBAL configpad_t configpad;//コンフィグで設定したキー情報
GLOBAL int tempkey[14];


GLOBAL int MouseX,MouseY;
GLOBAL int color[10];
//novel用
GLOBAL char StringBuf[ STRBUF_HEIGHT ][ STRBUF_WIDTH * 2 + 1 ] ;	// 仮想テキストバッファ
GLOBAL int CursorX , CursorY ;						// 仮想画面上での文字表示カーソルの位置
GLOBAL int SP , CP ,BP;							// 参照する文字列番号と文字列中の文字ポインタ、何ワード目か
GLOBAL int EndFlag ;							// 終了フラグ
GLOBAL int KeyWaitFlag ;						// ボタン押し待ちフラグ
GLOBAL int CharNum;//現在話してるキャラ
GLOBAL int LRFlag;//名前と画像を右に表示か左に表示か　0:左
GLOBAL char CharName[10][256];

//ゲーム内変数

GLOBAL int event;
GLOBAL int battleflag;
GLOBAL int bselector[2];
GLOBAL int miss;//セレクトしなかった
GLOBAL int soundf;
GLOBAL int attackf;
GLOBAL int damage;//何ダメージか
GLOBAL int direct;//ダメージの方向　0だと自分　1だと相手


GLOBAL int cards[2][15];//1から15の数字がそれぞれ入る
GLOBAL int nowcard[2][5];//現在のカード　1から15のカード番号が入る
GLOBAL int bturn;//何ターン目か
GLOBAL int hp[2];

GLOBAL PLAYER pl[2] ;		// プレイヤー情報の実体宣言
GLOBAL CAMERA cam ;		// カメラ情報の実体宣言
