#include "GV.h"

//文字列
char String[][500] =
{
"@　　これはある日のことであるBC#1#ああ　ひまでひまでたまらねえなBCよし、ともだちにでんわでもかけるかBC#0#プルルルルルル　ガチャBCR#2#はい　イノハラですBCL#1#おう、ノギリだB@きょうひまかいのはら？BCR#2#おう　ひまだB@くるならこいE",
"※　↓ボス前　※" ,
"@ふっふっふBCL#1#ふっきんは　おくないでやれよBCR#3#ふっ　ひびしょうじんなりB@ぶかつをさぼる　ふとどきなやから　コーチのわたしみずからてをくだそうBCわたしのなは　どのうえひらざえもんB@りゃくして……BCL#1#そのさきはいわせんぞ！E",
"※　↓ボス後　※",
"R#3#りゃくして　どのえもん　B@ぐふっBCL#1#おまえはこーちしっかくだBCこーちはやめて　かがわになりな",
"Cではさっそく的を狙って撃ってもらおうB" ,
"@ゲームスタートだBE",
} ;


// 改行関数
void Kaigyou( void )
{
	// 描画行位置を一つ下げる
	CursorY ++ ;

	// 描画列を最初に戻す
	CursorX = 0 ;


}


void novel(){
	char OneMojiBuf[ 3 ] ;	// １文字分一時記憶配列
	char NumBuf[4];//キャラ番保持用

	int i , j ;
	
	SetFontSize(MOJI_SIZE);

	// サウンドノベル風文字列描画処理を行う
	// ただし終了フラグが１だった場合は処理をしない
	if( EndFlag == 0 )
	{
		char  Moji ;

		// ボタン押し待ちフラグがたっていた場合はボタンが押されるまでここで終了
		if( KeyWaitFlag == 1 )
		{
			if( CheckStatePad(configpad.circle)==1 || CheckStateMouse(MouseLeft)==1 ) 
			{
				// ボタンが押されていたら解除
				KeyWaitFlag = 0 ;

			}
		}
		else
		{
			// 文字の描画
			Moji = String[ event ][ CP ] ;
			switch( Moji )
			{
			case '@' :	// 改行文字

				// 改行処理および参照文字位置を一つ進める
				Kaigyou() ;
				CP ++ ;

				break ;

			case 'B' :	// ボタン押し待ち文字

				// ボタン押し待ちフラグをたてる
				KeyWaitFlag = 1 ;
				CP ++ ;
				break ;

			case 'E' :	// 終了文字
				
				// 終了フラグを立てるおよび参照文字位置を一つ進める
				EndFlag=1;
				KeyWaitFlag = 1 ;
				CP ++ ;
				break ;

			case 'C' :	// クリア文字

				// 仮想テキストバッファを初期化して描画文字位置を初期位置に戻すおよび参照文字位置を一つ進める
				for( i = 0 ; i < STRBUF_HEIGHT ; i ++ )
				{
					for( j = 0 ; j < STRBUF_WIDTH * 2 ; j ++ )
					{
						StringBuf[ i ][ j ] = 0 ;
					}
				}

				CursorY = 0 ;
				CursorX = 0 ;
				CP ++ ;

				break ;
			case '#' : //人物会話文字 #で囲んだ数字をキャラ番に
				CP++;
				i=0;
				while(String[ event ][ CP ] !='#'){
					NumBuf[i]=String[ event ][ CP ];
					CP++;
					i++;
				}
				NumBuf[i]='\0';
				CP++;
				CharNum=atoi(NumBuf);
				break;
			case 'L' :
				LRFlag=0;
				CP++;
				break;
			case 'R' :
				LRFlag=1;
				CP++;
				break;
			default :	// その他の文字

				// １文字分抜き出す
				OneMojiBuf[ 0 ] = String[ event ][ CP ] ;
				OneMojiBuf[ 1 ] = String[ event ][ CP + 1 ] ;
				OneMojiBuf[ 2 ] = '\0' ;

				// １文字テキストバッファに代入
				StringBuf[ CursorY ][ CursorX * 2 ] = OneMojiBuf[ 0 ] ;
				StringBuf[ CursorY ][ CursorX * 2 + 1 ] = OneMojiBuf[ 1 ] ;

				// 参照文字位置を２バイト勧める
				CP += 2 ;

				// カーソルを一文字文進める
				CursorX ++ ;

				// テキストバッファ横幅からはみ出たら改行する
				if( CursorX >= STRBUF_WIDTH ) Kaigyou() ;
				// もしテキストバッファ縦幅がはみ出たらボタンを待ちクリアする
				if( CursorY >= STRBUF_HEIGHT ){
					CP -=2;
					String[event][CP]='B';
					String[event][CP+1]='C';
				}
				break ;
			}

			// 参照文字列の終端まで行っていたら終わる
			if( String[ event ][ CP ] == '\0' )
			{
				EndFlag=1;
				KeyWaitFlag = 1 ;
				CP = 0 ;
			}
		}
	}

	//バックの画像


	//誰かキャラの発言だったら
	if(CharNum!=0){

		DrawExtendGraph( 0+LRFlag*724 , 50 , 300+LRFlag*724 ,  768*2/3 , img_char[CharNum] , FALSE ) ;
		//名前用の枠を用意
		//SetDrawBlendMode( DX_BLENDMODE_ALPHA , 180 ) ;
		DrawExtendGraph( 0+LRFlag*774 , 768*2/3-80 , 250+LRFlag*774 ,  768*2/3 , img_board[8] , TRUE ) ;
		//SetDrawBlendMode( DX_BLENDMODE_NOBLEND , 0 ) ;
		DrawExtendGraph( 0+LRFlag*774 , 768*2/3-80 , 250+LRFlag*774 ,  768*2/3 , img_board[7] , TRUE ) ;
		//名前の表示
		DrawFormatString( 10+LRFlag*774, 768*2/3-60, color[0], "%s",CharName[CharNum] ) ;

	}

	// テキストバッファの描画	
	SetDrawBlendMode( DX_BLENDMODE_ALPHA , 180 ) ;
	DrawExtendGraph( 0 , 768*2/3 , 1024 ,  768 , img_board[8] , TRUE ) ;
	SetDrawBlendMode( DX_BLENDMODE_NOBLEND , 0 ) ;
	DrawExtendGraph( 0 , 768*2/3 , 1024 ,  768 , img_board[7] , TRUE ) ;
	
	for( i = 0 ; i < STRBUF_HEIGHT ; i ++ )
	{
		DrawString( 56 , i * MOJI_SIZE +768*2/3+56, StringBuf[ i ] , color[0] ) ;
	}

	//終了なら
	if(EndFlag==1){
		if( CheckStatePad(configpad.circle)==1 || CheckStateMouse(MouseLeft)==1){
			CP=0;
			EndFlag=0;
			// 仮想テキストバッファを初期化して描画文字位置を初期位置に戻すおよび参照文字位置を一つ進める
			for( i = 0 ; i < STRBUF_HEIGHT ; i ++ )
			{
				for( j = 0 ; j < STRBUF_WIDTH * 2 ; j ++ )
				{
					StringBuf[ i ][ j ] = 0 ;
				}
			}
			// 描画位置の初期位置セット
			CursorX = 0 ;
			CursorY = 0 ;
			// フレームカウンタ初期化
			KeyWaitFlag = 0;
			count=0;
			CharNum=0;
			LRFlag=0;
			//イベントを進め、init処理をする
			event++;
			func_state=99;

		}
	}
	
}