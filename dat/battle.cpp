#include "GV.h"



void binit(){
	int tmp,rnd;
	int i,j;
	//hpの初期化
	hp[0]=MAX_HP;
	hp[1]=MAX_HP;
	bturn=1;
	bselector[0]=-1;

	//カードをランダムに並べ替える
	for(i=0;i<15;i++){
		cards[0][i]=i+1;
		cards[1][i]=i+1;
	}
	for(j=0;j<2;j++){
		for(i=0;i<15;i++){
			rnd=rand()%15;
			tmp=cards[j][i];
			cards[j][i]=cards[j][rnd];
			cards[j][rnd]=tmp;
		}
	}

	//カードを配布
	for(j=0;j<2;j++){
		for(i=0;i<5;i++){
			nowcard[j][i]=cards[j][i];
		}
	}

	battleflag=1;
	soundf=0;
	attackf=0;
	count=0;
	miss=0;
	Player_Initialize();
	Camera_Initialize();
}

void gofight(){

	DrawRotaGraph( 256 , 400 , 0.8f, 0,img_battle[2] , TRUE , FALSE) ;
	DrawRotaGraph( 768 , 400 , 0.8f, 0,img_battle[3] , TRUE , FALSE) ;

	if(count<150){

	}
	else if(count==150){
		PlaySoundMem( sound_se[2] , DX_PLAYTYPE_BACK ) ;
	}
	else if(count<230){
		DrawRotaGraph( 512 , 170 , 0.8f *(230-count), 0,img_battle[0] , TRUE , FALSE) ;
	}
	else if(count<300){
		DrawRotaGraph( 512 , 170 , 0.8f, 0,img_battle[0] , TRUE , FALSE) ;
	}else{//曲の開始と戦闘の開始
		battleflag=2;
		PlaySoundMem( sound_bgm[0] , DX_PLAYTYPE_LOOP ) ;
	}
	//DrawRotaGraph( 512 , 150 , 0.8f, 0.0f,img_battle[0] , TRUE , FALSE) ;



	//battleflag=2;
}

void select(){
	int i;
	int tmp1,tmp2;

	tmp1=GetSoundCurrentTime(sound_bgm[0]);
	tmp2=GetSoundTotalTime(sound_bgm[0]);
	//DrawFormatString( 400, 50, color[4], "sample = %d   %d", tmp1,tmp2 ) ;


	//カードをクリック
	if(CheckStateMouse(MouseLeft)==1 && attackf==0){
		//位置確認
		for(i=0;i<5;i++){
			if(Mousesquare(1024/10*(i*2+1)-54 , 768*5/6-80 , 1024/10*(i*2+1)+54 , 768*5/6+80)==1){
				bselector[0]=i;
			}
		}
	}


	//ポージング
	if( CheckStatePad(configpad.left)==1){
		Player_PlayAnim(0,0);
	}
	else if(CheckStatePad(configpad.up)==1){
		Player_PlayAnim(0,1);
	}
	else if(CheckStatePad(configpad.right)==1){
		Player_PlayAnim(0,2);
	}
	else if(CheckStatePad(configpad.down)==1){
		Player_PlayAnim(0,3);
	}
	else if(CheckStatePad(configpad.circle)==1){
		Player_PlayAnim(0,4);
	}
	else if(CheckStatePad(configpad.cross)==1){
		Player_PlayAnim(0,5);
	}
	else if(CheckStatePad(configpad.square)==1){
		Player_PlayAnim(0,6);
	}
	else if(CheckStatePad(configpad.triangle)==1){
		Player_PlayAnim(0,7);
	}

	//タイムアウト
	if(soundf*4000+3500<tmp1 && tmp1<4000*(soundf+1) && attackf==0){
		if(soundf==3){
			soundf=0;
		}else{
			soundf++;
		}
		attackf=1;

	}

	//エネミーのモーション
	if((tmp1+200)%2000<30){
		Player_PlayAnim(1,rand()%8);
	}
}

void calc_damage(){
	int tmp;

	//もしミスならそのまま2+数字分ダメージ
	if(miss==1){
		direct=0;
		damage=3+(nowcard[1][bselector[1]]-1)%5;
	}
	//同種だったら
	else if((int)((nowcard[0][bselector[0]]-1)/5) == (int)((nowcard[1][bselector[1]]-1)/5)){
		//大きさを比べて
		if(nowcard[0][bselector[0]]<nowcard[1][bselector[1]]){
			direct=0;
			damage=nowcard[1][bselector[1]]-nowcard[0][bselector[0]];
		}
		else if(nowcard[0][bselector[0]]>nowcard[1][bselector[1]]){
			direct=1;
			damage=nowcard[0][bselector[0]]-nowcard[1][bselector[1]];
		}else{
			direct=-1;
		}
	}
	//異種だったら
	else{
		if((int)((nowcard[0][bselector[0]]-1)/5)==0){//自分剣
			if((int)((nowcard[1][bselector[1]]-1)/5)==1){//盾
				direct=0;
				tmp=(nowcard[1][bselector[1]]-1)%5-(nowcard[0][bselector[0]]-1)%5;
			}
			else if((int)((nowcard[1][bselector[1]]-1)/5)==2){//杖
				direct=1;
				tmp=(nowcard[0][bselector[0]]-1)%5-(nowcard[1][bselector[1]]-1)%5;
			}

		}
		else if((int)((nowcard[0][bselector[0]]-1)/5)==1){//自分盾
			if((int)((nowcard[1][bselector[1]]-1)/5)==0){//剣
				direct=1;
				tmp=(nowcard[0][bselector[0]]-1)%5-(nowcard[1][bselector[1]]-1)%5;
			}
			else if((int)((nowcard[1][bselector[1]]-1)/5)==2){//杖
				direct=0;
				tmp=(nowcard[1][bselector[1]]-1)%5-(nowcard[0][bselector[0]]-1)%5;
			}
		}
		else if((int)((nowcard[0][bselector[0]]-1)/5)==2){//自分杖
			if((int)((nowcard[1][bselector[1]]-1)/5)==0){//剣
				direct=0;
				tmp=(nowcard[1][bselector[1]]-1)%5-(nowcard[0][bselector[0]]-1)%5;
			}
			else if((int)((nowcard[1][bselector[1]]-1)/5)==1){//盾
				direct=1;
				tmp=(nowcard[0][bselector[0]]-1)%5-(nowcard[1][bselector[1]]-1)%5;
			}
		}
		
		//差がプラスかどうかで
		if(0<tmp){
			damage=2+tmp*2;
		}else{
			damage=2;
		}
	}

	//hpを減らす
	if(direct==0){
		hp[0]=hp[0]-damage;
	}
	else if(direct==1){
		hp[1]=hp[1]-damage;
	}

	//0以下は0に
	if(hp[0]<0){
		hp[0]=0;
	}
	if(hp[1]<0){
		hp[1]=0;
	}
}

void attack(){//攻撃判定とダメージ


	if(attackf!=0){
		if(attackf==1){//アタックフェーズに入った
			//エネミーのカード選択
			bselector[1]=rand()%5;
			//もしプレイヤーが選択してないなら
			if(bselector[0]==-1){
				miss=1;//ミスフラグを立てる
				bselector[0]=rand()%5;
			}
			//ターンが11を超えてる場合
			if(11<bturn){
				int i,j=0,tmp;
				//エネミー
				tmp=rand()%(16-bturn);
				for(i=0;i<5;i++){
					if(nowcard[1][i]!=-1){
						j++;
					}
					if(j==tmp+1){
						bselector[1]=i;
						break;
					}
				}
				//自分
				if(bselector[0]==-1 || nowcard[0][bselector[0]]==-1){
					miss=1;
					j=0;
					tmp=rand()%(16-bturn);
					for(i=0;i<5;i++){
						if(nowcard[0][i]!=-1){
							j++;
						}
						if(j==tmp+1){
							bselector[0]=i;
							break;
						}
					}
				}
			}

		}
		else if(attackf<20){//カードの表示
			if(miss==1){
				SetDrawBright( 100 , 100 , 100 ) ;
			}
			DrawRotaGraph( 512-60 , 768/3 , 0.5f*(20-attackf), 0.0f,img_card[(nowcard[0][bselector[0]]-1)/5][(nowcard[0][bselector[0]]-1)%5] , TRUE , FALSE) ;
			SetDrawBright( 255 , 255 , 255 ) ;
			DrawRotaGraph( 512+60 , 768/3 , 0.5f*(20-attackf), 0.0f,img_card[(nowcard[1][bselector[1]]-1)/5][(nowcard[1][bselector[1]]-1)%5] , TRUE , FALSE) ;
		}
		else if(attackf==20){//計算
			calc_damage();
			if(miss==1){
				SetDrawBright( 100 , 100 , 100 ) ;
			}
			DrawRotaGraph( 512-60 , 768/3 , 0.5f, 0.0f,img_card[(nowcard[0][bselector[0]]-1)/5][(nowcard[0][bselector[0]]-1)%5] , TRUE , FALSE) ;
			SetDrawBright( 255 , 255 , 255 ) ;
			DrawRotaGraph( 512+60 , 768/3 , 0.5f, 0.0f,img_card[(nowcard[1][bselector[1]]-1)/5][(nowcard[1][bselector[1]]-1)%5] , TRUE , FALSE) ;

			if(direct!=-1){
				DrawFormatString( 512-60+direct*120, 768/3+85, color[1], "%d", damage ) ;
			}
		}
		else if(attackf<100){
			if(miss==1){
				SetDrawBright( 100 , 100 , 100 ) ;
			}
			DrawRotaGraph( 512-60 , 768/3 , 0.5f, 0.0f,img_card[(nowcard[0][bselector[0]]-1)/5][(nowcard[0][bselector[0]]-1)%5] , TRUE , FALSE) ;
			SetDrawBright( 255 , 255 , 255 ) ;
			DrawRotaGraph( 512+60 , 768/3 , 0.5f, 0.0f,img_card[(nowcard[1][bselector[1]]-1)/5][(nowcard[1][bselector[1]]-1)%5] , TRUE , FALSE) ;
			if(direct!=-1){
				DrawFormatString( 512-60+direct*120, 768/3+85, color[1], "%d", damage ) ;
			}
		}else{
			//カードの入れ替え
			if(bturn<11){
				nowcard[0][bselector[0]]=cards[0][4+bturn];
				nowcard[1][bselector[1]]=cards[1][4+bturn];
			}else{
				nowcard[0][bselector[0]]=-1;
				nowcard[1][bselector[1]]=-1;
			}
			attackf=-1;
			miss=0;
			damage=0;
			bselector[0]=-1;
			bturn++;

			//ターンが終わっているかどちらかが0以下のとき
			if(bturn==16 || hp[0]<=0 || hp[1]<=0){
				battleflag=10;
				Player_PlayAnim(0,3);
				Player_PlayAnim(1,3);
				count=0;
				StopSoundMem( sound_bgm[0] ) ;
			}
		}

		attackf++;
	}
	


}

void battleend(){
	if(count==1){//アニメーションの開始
		if(hp[0]>hp[1]){
			Player_PlayAnim(0,8);
			Player_PlayAnim(1,9);
			PlaySoundMem( sound_bgm[1] , DX_PLAYTYPE_BACK ) ;
		}
		else if(hp[0]<hp[1]){
			Player_PlayAnim(0,9);
			Player_PlayAnim(1,8);
			PlaySoundMem( sound_bgm[2] , DX_PLAYTYPE_BACK ) ;
		}else{
			PlaySoundMem( sound_bgm[3] , DX_PLAYTYPE_BACK ) ;
		}
	}
	else if(count<300){
		if(hp[0]>hp[1]){
			DrawRotaGraph( 512 , 485 , 1.0f, 0.0f,img_battle[4] , TRUE , FALSE) ;
		}
		else if(hp[0]<hp[1]){
			DrawRotaGraph( 512 , 485 , 1.0f, 0.0f,img_battle[5] , TRUE , FALSE) ;
		}
		else{
			DrawRotaGraph( 512 , 485 , 1.0f, 0.0f,img_battle[6] , TRUE , FALSE) ;
		}
	}
	else if(count==300){
		count=0;
		battleflag=0;
		func_state=90;
		Player_Terminate();
	}

}

void battle(){

	battlegraph();

	switch(battleflag){
	case 0://カードを配るとかはじめのinit
		binit();
		break;
	case 1://カードを表示と対戦開始の合図
		gofight();
		break;
	case 2://カードの提示　意味もなく踊る
		select();
		//攻撃判定
		attack();
		break;
	case 10:
		battleend();
		break;
	}


}